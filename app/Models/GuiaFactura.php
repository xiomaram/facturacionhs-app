<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GuiaFactura extends Model
{
    use HasFactory;
    protected $fillable = ['factura_id','guia_id'];
}
