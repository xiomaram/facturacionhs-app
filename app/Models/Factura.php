<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    
    use HasFactory;
    static $rules = [
		'establecimiento' => 'required',
		'punto_emision' => 'required',
		'secuencial' => 'required',
		'fecha_emision' => 'required',
		'subtotal' => 'required',
		'impuesto' => 'required',
		'total' => 'required',
    ];

   
    protected $perPage = 20;
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['establecimiento','punto_emision','secuencial','fecha_emision','subtotal','impuesto','total'];

    /**
     *Obtener todas las guías de unafactura
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function guias()
    {
      return $this->hasMany("App\Guia", "guia_id");
    }


}
