<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guia extends Model
{
    use HasFactory;
    static $rules = [
		'numero_guia' => 'required',
		'fecha_envio' => 'required',
		'pais_origen' => 'required',
		'nombre_remitente' => 'required',
		'direccion_remitente' => 'required',
		'pais_destino' => 'required',
		'nombre_destinatario' => 'required',
		'direccion_destinatario' => 'required',
		'total' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['numero_guia','fecha_envio','pais_origen','nombre_remitente','direccion_remitente','telefono_remitente','email_remitente','pais_destino','nombre_destinatario','direccion_destinatario','telefono_destinatario','email_destinatario','total'];

    public function factura()
    {
        return $this->belongsTo(Guia::class);
    }
}
