<?php

namespace App\Http\Controllers;

use App\Models\Factura;
use Illuminate\Http\Request;
use App\Models\Guia;
use App\Models\GuiaFactura;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class FacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facturas = Factura::paginate();

        return view('factura.index', compact('facturas'))
            ->with('i', (request()->input('page', 1) - 1) * $facturas->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
        $factura = new Factura();
        return view('factura.create', compact('factura'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*public function store(Request $request)
    {
        //
        request()->validate(Factura::$rules);

        $factura = Factura::create($request->all());

        return redirect()->route('facturas.index')
            ->with('success', 'Factura creada con éxito.');
    }*/

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $factura = Factura::find($id);
        
        return view('factura.show', compact('factura'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */


     /*****Metodo para editar una Factura*** */
    public function edit($id)
    {
        //
        $data=null;
        $factura = Factura::find($id);

        $data['factura']=$factura;  
        $data_guia=null;     

        $guias = Guia::select('guias.*')        
        ->leftJoin('guia_facturas','guias.id','guia_facturas.guia_id') 
        ->leftJoin('facturas','facturas.id','guia_facturas.factura_id')       
        ->where('facturas.id',$id)->get();         
        
        if($guias){
            //recorro la coleccion dolar guias para almacenar en uuna variable array los objetos 
            //de las guias que contiene la factura a editar
            foreach($guias as $key=>$guia)            {

                $data_guia[$key]=$guia;               
                
            } 
            //envio este arreglo a la funcion guardarGuias para almanenar los datos en 
            //una variable de sesión y retornarla a la vista factura.edit           
            $this->guardarGuias($data_guia);            
        }      
        
        return view('factura.edit')->with($data);
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        request()->validate(Factura::$rules);

        $factura->update($request->all());

        return redirect()->route('facturas.index')
            ->with('success', 'Factura actualizada con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Factura  $factura
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       
        $factura = Factura::find($id)->delete();

        return redirect()->route('facturas.index')
            ->with('success', 'Factura eliminada con éxito');
    }

    //Facturar las guías

    public function facturarCancelar(Request $request)
    {
        if ($request->input("accion") == "facturar") {
          
            return $this->facturarGuia($request);
        } else {
            return $this->cancelarFacturacion();
        }
    }


    public function facturarGuia(Request $request)
    {
        // Crear una Nueva Factura
       
        $factura = new Factura();

        request()->validate(Factura::$rules);

        $factura->establecimiento = $request->input("establecimiento");
        $factura->punto_emision = $request->input("punto_emision");
        $factura->secuencial = $request->input("secuencial");
        $factura->fecha_emision = $request->input("fecha_emision");
        $factura->subtotal = $request->input("subtotal");
        $factura->impuesto = $request->input("impuesto");
        $factura->total = $request->input("total"); 
        
        
        $guias = $this->obtenerGuias();  
        $factura->saveOrFail();
        $idFactura = $factura->id;
        //$guias = $this->obtenerGuias();   
        
        // Recorrer Las Guias a facturar
        foreach ($guias as $guia) {
            // Guia a facturar
            $guiaFactura = new GuiaFactura();
            $guiaFactura->fill([
                "factura_id" => $idFactura,
                "guia_id" => $guia->id,
                
            ]);
            // Lo guardamos
            $guiaFactura->saveOrFail();
            // Actualizamos el estatus de las guias
            $guiaActualizada = Guia::find($guia->id);
            $guiaActualizada->estatus = 'Facturada';
            $guiaActualizada->saveOrFail();
        }
        $this->vaciarGuias();
        return redirect()
            ->route("facturas.index")
            ->with("mensaje", "Facturación Finalizada");
    }
    //Función para obtener las guías que se van a facturar
    //lista de Guias para la Facturación, se guardan en una sesión como un arreglo
    private function obtenerGuias()
    {
        $guias = session("guias");
        if (!$guias) {
            $guias = [];
        }
      
        return $guias;
    }

    private function vaciarGuias()
    {
        $this->guardarGuias(null);
    }

    private function guardarGuias($guias)
    {
        session(["guias" => $guias,
        ]);        
    }


    public function cancelarFacturacion()
    {
        $this->vaciarGuias();
        return redirect()
            ->route("facturas.index")
            ->with("mensaje", "Facturación cancelada");
    }
    public function quitarGuiaDeLista(Request $request)
    {
        $indice = $request->post("indice");
        $guias = $this->obtenerGuias();
        array_splice($guias, $indice, 1);
        $this->guardarGuias($guias);
        return redirect()
            ->route("facturas.create");
    }


    /* Agregar guías  la lista para la facturacion*/
    public function agregarGuiaALista(Request $request)
    {
        $codigo = $request->post("codigo");
        $guia = Guia::where("numero_guia", "=", $codigo)->first();
        if (!$guia) {
            return redirect()
                ->route("facturas.create")
                ->with("mensaje", "Guía no encontrada");
        }
        $this->agregarGuiaAFactura($guia);
        
        return redirect()
            ->route("facturas.create");
    }

    private function agregarGuiaAFactura($guia)
    {
        if ($guia->estatus == 'Facturada') {
            return redirect()->route("facturas.create")
                ->with([
                    "mensaje" => "Esta guía ya ha sido Facturada",
                    "tipo" => "danger"
                ]);
        }
        $guias = $this->obtenerGuias();   
             
        $posibleIndice = $this->buscarIndiceDeGuia($guia->numero_guia, $guias);
        // Es decir, guia no fue encontrada
        if($posibleIndice === -1) {            
            array_push($guias, $guia);
        } 
        else {
            return redirect()->route("facturas.create")
                ->with('error', 'Ya esta guía ha sido agregada');
        }
        $this->guardarGuias($guias);

       
        
    }

    /*
    *Editar la factura
    */

    /* Agregar guías  la lista para la facturacion*/
    public function agregarGuiaAListaEdit(Request $request)
    {
        $codigo = $request->post("codigo");
        $guia = Guia::where("numero_guia", "=", $codigo)->first();
        if (!$guia) {
            return redirect()
                ->route("facturas.edit")
                ->with("mensaje", "Guía no encontrada");
        }
        $this->agregarGuiaAFacturaEdit($guia);
        
        return redirect()->back();
            //->route("facturas.edit");
    }

    private function agregarGuiaAFacturaEdit($guia)
    {
        if ($guia->estatus == 'Facturada') {
            return redirect()->back()
            //->route("facturas.edit")
                ->with([
                    "mensaje" => "Esta guía ya ha sido Facturada",
                    "tipo" => "danger"
                ]);
        }
        $guias = $this->obtenerGuias();   
             
        $posibleIndice = $this->buscarIndiceDeGuia($guia->numero_guia, $guias);
        // Es decir, guia no fue encontrada
        if($posibleIndice === -1) {            
            array_push($guias, $guia);  
            //$this->guardarGuias($guias);  
                  
           
        } 
        else {
            return redirect()->back()
            //->route("facturas.create")
                ->with('error', 'Ya esta guía ha sido agregada');
        }
       
       
        

       
        
    }





    //Verifica si el número de guia ya esta agregado en la lista de facturación
    private function buscarIndiceDeGuia(string $codigo, array &$guias)
    {
        foreach ($guias as $indice => $guia) {
            if ($guia->numero_guia === $codigo) {
                return $indice;
            }
        }
        return -1;
    }

}
