<?php

namespace App\Http\Controllers;

use App\Models\TipoPago;
use Illuminate\Http\Request;

class TipoPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tipopagos = Tipopago::paginate();

        return view('tipopago.index', compact('tipopagos'))
            ->with('i', (request()->input('page', 1) - 1) * $tipopagos->perPage());
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tipopago = new Tipopago();
        return view('tipopago.create', compact('tipopago'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        request()->validate(Tipopago::$rules);

        $tipopago = Tipopago::create($request->all());

        return redirect()->route('tipopagos.index')
            ->with('success', 'Tipo de pago creado con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TipoPago  $tipoPago
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tipopago = Tipopago::find($id);

        return view('tipopago.show', compact('tipopago'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TipoPago  $tipoPago
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tipopago = Tipopago::find($id);

        return view('tipopago.edit', compact('tipopago'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TipoPago  $tipoPago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        request()->validate(Tipopago::$rules);
        $tipopago = TipoPago::find($id);

        $tipopago->update($request->all());

        return redirect()->route('tipopagos.index')
            ->with('success', 'Tipo de pago actualizado con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TipoPago  $tipoPago
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tipopago = TipoPago::find($id)->delete();

        return redirect()->route('tipopagos.index')
            ->with('success', 'Tipo de pago eliminado con éxito');
    }
}
