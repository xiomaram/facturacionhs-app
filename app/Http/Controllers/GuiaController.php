<?php

namespace App\Http\Controllers;

use App\Models\Guia;
use Illuminate\Http\Request;
use Dompdf\Dompdf;
use PDF;

class GuiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $guias = Guia::paginate();
        //dd($guias);

        return view('guia.index', compact('guias'))
            ->with('i', (request()->input('page', 1) - 1) * $guias->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $guia = new Guia();
        return view('guia.create', compact('guia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        request()->validate(Guia::$rules);

        $guia = Guia::create($request->all());

        return redirect()->route('guias.index')
            ->with('success', 'Guia created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Guia  $guia
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //       
        $guia = Guia::find($id);  
        return view('guia.show', compact('guia'));

      
    }
    public function createGuiaPDF($id)
    {
            //Recuperar datos de la guia seleccionada de la base de datos
            $guia = Guia::find($id);          
            $pdf = new DOMPDF();
            $pdf = PDF::loadView('guia.pdf',compact('guia') )->setPaper('a4', 'portrait'); 
            return $pdf->stream();           
            return $pdf->download('guia.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Guia  $guia
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $guia = Guia::find($id);  
        return view('guia.edit', compact('guia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Guia  $guia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        //
        request()->validate(Guia::$rules);
        $guia = Guia::find($id);
       
        if($guia)
        {
            $guia->update($request->all());

            return redirect()->route('guias.index')
            ->with('success', 'Guia actualizada con éxito');
        }
        else{
            return redirect()->route('guias.index')
            ->with('error', 'Guia no ha sido encontrada');         
            
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Guia  $guia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $guia = Guia::find($id)->delete();
        
        return redirect()->route('guias.index')
            ->with('success', 'Guia deleted successfully');
    }
}
