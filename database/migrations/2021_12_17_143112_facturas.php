<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Facturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('facturas', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->string('establecimiento', 3);
            $table->string('punto_emision', 3);
            $table->integer('secuencial')->unsigned();
            $table->dateTime('fecha_emision');
            $table->decimal('subtotal', 18, 2);
            $table->decimal('impuesto', 18, 2);
            $table->decimal('total', 18, 2);           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('facturas');
    }
}
