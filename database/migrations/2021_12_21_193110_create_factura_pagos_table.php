<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura_pagos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('factura_id')->unsigned();            
            $table->foreign('factura_id')->references('id')->on('facturas')->onDelete('cascade');  
            $table->bigInteger('tipopago_id')->unsigned();  
            $table->foreign('tipopago_id')->references('id')->on('tipo_pagos')->onDelete('cascade');
            $table->decimal('Valor', 18, 2);       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura_pagos');
    }
}
