<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Guias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('guias', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('numero_guia', 10);
            $table->dateTime('fecha_envio');
            $table->string('pais_origen', 100);
            $table->string('nombre_remitente', 100);
            $table->string('direccion_remitente', 100);
            $table->string('telefono_remitente', 50)->nullable();
            $table->string('email_remitente', 50)->nullable();
            $table->string('pais_destino', 100);
            $table->string('nombre_destinatario', 100);
            $table->string('direccion_destinatario', 100);
            $table->string('telefono_destinatario', 50)->nullable();
            $table->string('email_destinatario', 50)->nullable();
            $table->decimal('total', 18, 2); 
            $table->string('estatus', 15)->nullable()->default('SIN FACTURAR');                   
            $table->timestamps();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('guias');
    }
}
