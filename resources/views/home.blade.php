@extends('layouts.layout_m')

@section('content')
    <div class="col-12 text-center">
        <h1>Bienvenido, Xiomara</h1>
    </div>
    @foreach([
        ["tipopagos", "guias", "facturas"],
        []
        ] as $modulos)
            <div class="col-12 pb-2 py-5">
                <div class="row text-center">
                    @foreach($modulos as $modulo)
                        <div class="col-12 col-md-4">
                            <div class="card">                                
                               {{-- <img class="card-img-top" src="{{url("/img/$modulo.png")}}">--}}                               
                                <div class="card-body">                                   
                                    <a href="{{route("$modulo.index")}}" class="btn btn-success">
                                        Ir a&nbsp;{{$modulo === "tipopagos" ? "Tipo de Pagos" : ucwords($modulo)}}
                                        <i class="fa fa-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
   
@endsection