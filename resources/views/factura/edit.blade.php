@extends('layouts.layout_m')

@section('template_title')
    Actualizar Factura
@endsection

@section('content')

<section class="content container-fluid">
   {{-- Creacion de la variable subtotal para sumar el total de las guias a facturar--}}
    @php
    $subtotal=0;
    $impuesto=0;
    $total=0;
    @endphp   

    <div class="row">
        <div class="col-12">
            <h4>Facturación <i class="fa fa-cart-plus"></i></h4>
            <div class="row">              
                <div class="col-12 col-md-6 py-2">
                    <form action="{{route ('facturas.agregarGuiaEdit')}}" method="post">
                        @csrf
                        <div class="form-group">                            
                            <input id="codigo" autocomplete="off" required autofocus name="codigo" type="text" class="form-control" placeholder="Número de guía">
                        </div>
                    </form>
                </div>                
                <div class="col-12 col-md-6 py-2">  
                    <form action="{{route("facturas.facturarCancelar")}}" method="post">
                        @csrf
                        @if(session("guias") !== null)  
                        <div class="form-group">
                            <button name="accion" value="cancelar" type="submit" class="btn btn-danger">Cancelar</button>
                        </div> 
                         @endif
                    </form>        
                </div>
           </div>
           @if(session("guias") !== null)
            <div class="row">
                <div class="col-12 col-md-8">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Número/Guía</th>                       
                                <th>País/Origen</th>
                                <th>Remitente</th>
                                <th>País/Destino</th>
                                <th>Destinatario</th>                       
                                <th>Total</th>                        
                                <th>Quitar</th>
                            </tr>
                            </thead>
                            <tbody>
                               
                            @foreach(session("guias") as $guia)                           
                                <tr>
                                    <td>{{$guia->numero_guia}}</td>    
                                    <td>{{$guia->pais_origen}}</td>
                                    <td>{{$guia->nombre_remitente}}</td>
                                    <td>{{$guia->pais_destino}}</td>
                                    <td>{{$guia->nombre_destinatario}}</td>      
                                    <td>{{number_format($guia->total, 2)}}</td>                           
                                    <td>
                                        <form action="{{route("facturas.quitarGuia")}}" method="post">
                                            @method("delete")
                                            @csrf
                                            <input type="hidden" name="indice" value="{{$loop->index}}">
                                            <button type="submit" class="btn btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>   
                                                      
                                <tr>
                                    <td class="text-end" colspan="5">                               
                                      <strong>Total</strong>  
                                    </td>
                                    <td> 
                                        @foreach(session("guias") as $guia)  
                                        @php
                                        $subtotal+=$guia->total;
                                        $impuesto=$subtotal*0.12;
                                        $total=$subtotal+$impuesto;
                                        @endphp             
                                        @endforeach
                                      <strong>{{number_format( $subtotal,2,',','.') }}   </strong>                   
                                    </td>        
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                   <form action="{{ route('facturas.facturarCancelar') }}" method="post">
                    @csrf    
                    <input type="hidden" name="id" value="{{ $factura->id }}">                
                    @if(session("guias") !== null)                       
                        <div class="row">
                            <div class="col-md-12">                
                                @includeif('partials.errors')                
                                <div class="card card-default">
                                    <div class="card-header">
                                        <span class="card-title">Datos de la Factura</span>
                                    </div>
                                    <div class="card-body">  
                                        @method('PUT')
                                        @csrf
                                        @if(empty($subtotal)) 
                                        @php
                                        $subtotal=0;
                                        $subtotal=$factura->subtotal;;
                                        @endphp
                                        @else
                                        @php
                                        $subtotal=$subtotal;               
                                        @endphp
                                        @endif
                                    
                                        @if(empty($impuesto)) 
                                        @php
                                        $impuesto=0;
                                        $impuesto=$factura->impuesto;;
                                        @endphp
                                        @else
                                        @php
                                        $impuesto=$impuesto;               
                                        @endphp
                                        @endif
                                    
                                        @if(empty($total)) 
                                        @php
                                        $total=0;
                                        $total=$factura->total;;
                                        @endphp
                                        @else
                                        @php
                                        $impuesto=$impuesto;               
                                        @endphp
                                        @endif
                                        @include('factura.form')
            
                                    </div>
                                </div>
                            </div>                           
                        </div>
                        <div class="form-group text-end py-1">
                            <button name="accion" value="facturar" type="submit" class="btn btn-success">Facturar</button>                            
                        </div> 
                    @endif
                    </form>
                </div>                
            </div>          
      
        @else
            <h4>Guias a Facturar</h4>                
        @endif

        </div>
    </div>
</section>
@endsection




