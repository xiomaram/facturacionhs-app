@extends('layouts.layout_m')

@section('template_title')
    
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Mostrar Factura</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('facturas.index') }}"> Volver</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Establecimiento:</strong>
                            {{ $factura->establecimiento }}
                        </div>
                        <div class="form-group">
                            <strong>Punto Emision:</strong>
                            {{ $factura->punto_emision }}
                        </div>
                        <div class="form-group">
                            <strong>Secuencial:</strong>
                            {{ $factura->secuencial }}
                        </div>
                        <div class="form-group">
                            <strong>Fecha de emisión:</strong>
                            {{ $factura->fecha_emision }}
                        </div>
                        <div class="form-group">
                            <strong>Subtotal:</strong>
                            {{ $factura->subtotal }}
                        </div>
                        <div class="form-group">
                            <strong>Impuesto:</strong>
                            {{ $factura->impuesto }}
                        </div>
                        <div class="form-group">
                            <strong>Total:</strong>
                            {{ $factura->total }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
