@extends('layouts.layout_m')

@section('template_title')
    Factura
@endsection

@section('content')
    <div class="container-fluid table-personl">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center" >

                            <span id="card_title">
                                {{ __('Factura') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('facturas.create') }}" class="btn btn-facture-1 btn-sm float-right"  data-placement="left">
                                  {{ __('Crear Factura') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>N°</th>                                        
										<th>Establecimiento</th>
										<th>Punto/Emisión</th>
										<th>Secuencial</th>
										<th>Fecha/Emisión</th>
										<th>Subtotal</th>
										<th>Impuesto</th>
										<th>Total</th>
                                        <th style="min-width: 150px;">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($facturas as $factura)
                                        <tr>
                                            <td>{{ ++$i }}</td>                                            
											<td>{{ $factura->establecimiento }}</td>
											<td>{{ $factura->punto_emision }}</td>
											<td>{{ $factura->secuencial }}</td>
											<td>{{ $factura->fecha_emision }}</td>
											<td>{{ $factura->subtotal }}</td>
											<td>{{ $factura->impuesto }}</td>
											<td>{{ $factura->total }}</td>

                                            <td>

                                                <div class="row">
                                                    <div class="col-2">
                                                        <a class="btn p-0  text-primary " href="{{ route('facturas.show',$factura->id) }}"><i class="fas fa-eye"></i></a>
                                                    </div>
                                                    <div class="col-2">
                                                        <a class="btn p-0  text-success" href="{{ route('facturas.edit',$factura->id) }}"><i class="fa fa-fw fa-edit"></i></a>                                                
                                                    </div>
                                                    <div class="col-2">
                                                        <form action="{{ route('facturas.destroy',$factura->id) }}" method="POST">                                                   
                                                            @csrf
                                                            {{@method_field('DELETE')}}
                                                            <button type="submit" class="btn p-0 text-danger"><i class="fa fa-fw fa-trash"></i></button>
                                                        </form>
                                                    </div>
                                                </div>

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $facturas->links() !!}
            </div>
        </div>
    </div>
@endsection
