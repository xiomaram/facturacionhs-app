<div class="box box-info padding-1">
    <div class="box-body">
        <div class="col py-1">
            <div class="form-group">                   
                <input class="form-control {{ $errors->has('establecimiento') ? ' is-invalid' : '' }}" value="{{ old('establecimiento') ? old('establecimiento') : $factura->establecimiento }}" placeholder="Establecimiento" name="establecimiento" type="text" required autocomplete="off" minlength="1" maxlength="3">
                @if ($errors->has('establecimiento'))
                <div class="invalid-feedback">{{ $errors->first('establecimiento') }}</div>
                @endif
            </div>              
        </div>
        <div class="col py-1">
            <div class="form-group">                   
                <input class="form-control {{ $errors->has('punto_emision') ? ' is-invalid' : '' }}" value="{{ old('punto_emision') ? old('punto_emision') : $factura->punto_emision }}" placeholder="Punto de emisión" name="punto_emision" type="text" required autocomplete="off">
                @if ($errors->has('punto_emision'))
                <div class="invalid-feedback">{{ $errors->first('punto_emision') }}</div>
                @endif
            </div>              
        </div>
        
        <div class="col py-1">
            <div class="form-group">                   
                <input class="form-control {{ $errors->has('secuencial') ? ' is-invalid' : '' }}" value="{{ old('secuencial') ? old('secuencial') : $factura->secuencial }}" placeholder="Secuencial" name="secuencial" type="number" min="1" required autocomplete="off">
                @if ($errors->has('secuencial'))
                <div class="invalid-feedback">{{ $errors->first('secuencial') }}</div>
                @endif
            </div>              
        </div>

        <div class="col py-1">
            <div class="form-group">                   
                <input class="form-control {{ $errors->has('fecha_emision') ? ' is-invalid' : '' }}" value="{{ old('fecha_emision') ? old('fecha_emision') : $factura->fecha_emision }}" placeholder="Fecha de emisión" name="fecha_emision" required autocomplete="off" type="text" id="datetimepickerFechaEmision"/>   
                @if ($errors->has('fecha_envio'))
                <div class="invalid-feedback">{{ $errors->first('fecha_envio') }}</div>
                @endif
            </div>
        </div>

        <div class="col py-1">
            <div class="form-group">                  
                <input type="hidden" class="">                 
                <input class="form-control {{ $errors->has('subtotal') ? ' is-invalid' : '' }}" value="{{ old('subtotal') ? old('subtotal') : $subtotal }}" placeholder="Subtotal" name="subtotal" type="number" step="0.01" min="0" required autocomplete="off" readonly>
                @if ($errors->has('subtotal'))
                <div class="invalid-feedback">{{ $errors->first('subtotal') }}</div>
                @endif
            </div>              
        </div>  


        <div class="col py-1">
            <div class="form-group">                   
                <input class="form-control {{ $errors->has('impuesto') ? ' is-invalid' : '' }}" value="{{ old('impuesto') ? old('subtotal') : $impuesto }}" placeholder="Impuesto" name="impuesto" type="number" step="0.01" min="0" maxlength="20" required autocomplete="off" readonly>
                @if ($errors->has('impuesto'))
                <div class="invalid-feedback">{{ $errors->first('impuesto') }}</div>
                @endif
            </div>              
        </div>

        <div class="col py-1">
            <div class="form-group">                   
                <input class="form-control {{ $errors->has('total') ? ' is-invalid' : '' }}" value="{{ old('total') ? old('total') : $total }}" placeholder="Total" name="total" type="number" step="0.01" min="0" required autocomplete="off" readonly>
                @if ($errors->has('total'))
                <div class="invalid-feedback">{{ $errors->first('total') }}</div>
                @endif
            </div>              
        </div>      
    </div> 
</div>