@extends('layouts.layout_m')
@section('template_title')
    Guia
@endsection

@section('content')
    <div class="container-fluid table-personl">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">

                            <span id="card_title">
                                {{ __('Guía') }}
                            </span>

                             <div class="float-right">
                                <a href="{{route('guias.create')}}" class="btn btn-facture-1 btn-sm float-right"  data-placement="left">
                                  {{ __('Crear Guía') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead class="thead">
                                    <tr>
                                        <th>N°</th>                                        
										<th>Numero/Guía</th>
										<th>Fecha/Envio</th>
										<th>País/Origen</th>
										<th>Nombre/Remitente</th>
										<th>Dirección/Remitente</th>
										<th>Teléfono/Remitente</th>
										<th>Email/Remitente</th>
										<th>País/Destino</th>
										<th>Nombre/Destinatario</th>
										<th>Dirección/Destinatario</th>
										<th>Teléfono/Destinatario</th>
										<th>Email/Destinatario</th>
										<th>Total</th>
                                        <th style="min-width: 150px;">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($guias as $guia)
                                        <tr>
                                            <td>{{ ++$i }}</td>                                            
											<td>{{ $guia->numero_guia }}</td>
											<td>{{ $guia->fecha_envio }}</td>
											<td>{{ $guia->pais_origen }}</td>
											<td>{{ $guia->nombre_remitente }}</td>
											<td>{{ $guia->direccion_remitente }}</td>
											<td>{{ $guia->telefono_remitente }}</td>
											<td>{{ $guia->email_remitente }}</td>
											<td>{{ $guia->pais_destino }}</td>
											<td>{{ $guia->nombre_destinatario }}</td>
											<td>{{ $guia->direccion_destinatario }}</td>
											<td>{{ $guia->telefono_destinatario }}</td>
											<td>{{ $guia->email_destinatario }}</td>
											<td>{{ $guia->total }}</td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-2">
                                                        <a class="btn p-0  text-primary " href="{{ route('guias.show',$guia->id) }}"><i class="fas fa-eye"></i></a>
                                                    </div>
                                                    <div class="col-2">
                                                        <a class="btn p-0  text-success" href="{{ route('guias.edit',$guia->id) }}"><i class="fa fa-fw fa-edit"></i></a>                                                
                                                    </div>
                                                    <div class="col-2">
                                                        <form action="{{ route('guias.destroy',$guia->id) }}" method="POST">                                                   
                                                            @csrf
                                                            {{@method_field('DELETE')}}
                                                            <button type="submit" class="btn p-0 text-danger"><i class="fa fa-fw fa-trash"></i></button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $guias->links() !!}
            </div>
        </div>
    </div>
@endsection