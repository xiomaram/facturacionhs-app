<div class="box box-info padding-1">
    <div class="box-body">        
        <div class="row g-3 py-1">
            <div class="col">
                <div class="form-group">
                    {{--<label for="N° Guía">N° Guía</label>--}}
                    <input class="form-control {{ $errors->has('numero_guia') ? ' is-invalid' : '' }}" value="{{ old('numero_guia') ? old('numero_guia') : $guia->numero_guia }}" placeholder="Número de guía" name="numero_guia" type="text" required autocomplete="off" minlength="1" maxlength="10">
                    @if ($errors->has('numero_guia'))
                    <div class="invalid-feedback">{{ $errors->first('numero_guia') }}</div>
                    @endif
                </div>              
            </div>
            <div class="col">
                <div class="form-group">                   
                   <input class="form-control {{ $errors->has('fecha_envio') ? ' is-invalid' : '' }}" value="{{ old('fecha_envio') ? old('fecha_envio') : $guia->fecha_envio }}" placeholder="Fecha de envío" name="fecha_envio" required type="text" id="datetimepicker"/>   
                    @if ($errors->has('fecha_envio'))
                    <div class="invalid-feedback">{{ $errors->first('fecha_envio') }}</div>
                    @endif
                </div>
            </div>
        </div> 
        <div class="row g-3 py-1">
            <div class="col">
                <div class="form-group">
                    {{--<label for="País origen">País Origen</label>--}}
                    <input class="form-control {{ $errors->has('pais_origen') ? ' is-invalid' : '' }}" value="{{ old('pais_origen') ? old('pais_origen') : $guia->pais_origen }}" placeholder="País de origen" name="pais_origen" type="text" maxlength="100" required autocomplete="off">
                    @if ($errors->has('pais_origen'))
                    <div class="invalid-feedback">{{ $errors->first('pais_origen') }}</div>
                    @endif
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    {{--<label for="Nombre remitente">Nombre Remitente</label>--}}
                    <input class="form-control {{ $errors->has('nombre_remitente') ? ' is-invalid' : '' }}" value="{{ old('nombre_remitente') ? old('nombre_remitente') : $guia->nombre_remitente }}" placeholder="Nombre del remitente" name="nombre_remitente" type="text" maxlength="100" required autocomplete="off">
                    @if ($errors->has('nombre_remitente'))
                    <div class="invalid-feedback">{{ $errors->first('nombre_remitente') }}</div>
                    @endif
                </div>
            </div>
        </div>  


        <div class="row g-3 py-1">
            <div class="col">
                <div class="form-group">
                    {{--<label for="Dirección del remitente">Dirección Del Remitente</label>--}}
                    <input class="form-control {{ $errors->has('direccion_remitente') ? ' is-invalid' : '' }}" value="{{ old('direccion_remitente') ? old('direccion_remitente') : $guia->direccion_remitente }}" placeholder="Dirección del remitente" name="direccion_remitente" type="text" maxlength="100" required autocomplete="off">
                    @if ($errors->has('direccion_remitente'))
                    <div class="invalid-feedback">{{ $errors->first('direccion_remitente') }}</div>
                    @endif
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    {{--<label for="Teléfono del remitente">Teléfono Del Remitente</label>--}}
                    <input class="form-control {{ $errors->has('telefono_remitente') ? ' is-invalid' : '' }}" value="{{ old('telefono_remitente') ? old('telefono_remitente') : $guia->telefono_remitente }}" placeholder="Teléfono del remitente" name="telefono_remitente" type="text" maxlength="50">
                    @if ($errors->has('telefono_remitente'))
                    <div class="invalid-feedback">{{ $errors->first('telefono_remitente') }}</div>
                    @endif
                </div>
            </div>
        </div>       

            
        <div class="row g-3 py-1">
            <div class="col">
                <div class="form-group">
                    {{--<label for="Email del remitente">Email Del Remitente</label>--}}
                    <input class="form-control {{ $errors->has('email_remitente') ? ' is-invalid' : '' }}" value="{{ old('email_remitente') ? old('email_remitente') : $guia->email_remitente }}" placeholder="Email del remitente" name="email_remitente" type="email" maxlength="50">
                    @if ($errors->has('email_remitente'))
                    <div class="invalid-feedback">{{ $errors->first('email_remitente') }}</div>
                    @endif
                </div>                
            </div>
            <div class="col">
                <div class="form-group">
                    {{--<label for="País de destino">País De Destino</label>--}}
                    <input class="form-control {{ $errors->has('pais_destino') ? ' is-invalid' : '' }}" value="{{ old('pais_destino') ? old('pais_destino') : $guia->pais_destino }}" placeholder="País de destino" name="pais_destino" type="text" maxlength="100" required autocomplete="off">
                    @if ($errors->has('pais_destino'))
                    <div class="invalid-feedback">{{ $errors->first('pais_destino') }}</div>
                    @endif
                </div>
            </div>
        </div>        
      
        
        <div class="row g-3 py-1">
            <div class="col">
                <div class="form-group">
                   {{--<label for="Nombre del destinatario">Nombre Del Destinatario</label>--}}
                    <input class="form-control {{ $errors->has('nombre_destinatario') ? ' is-invalid' : '' }}" value="{{ old('nombre_destinatario') ? old('nombre_destinatario') : $guia->nombre_destinatario }}" placeholder="Nombre del destinatario" name="nombre_destinatario" type="text" maxlength="100" required autocomplete="off">
                    @if ($errors->has('nombre_destinatario'))
                    <div class="invalid-feedback">{{ $errors->first('nombre_destinatario') }}</div>
                    @endif
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    {{--<label for="Dirección del destinatario">Dirección Del Destinatario</label>---}}
                    <input class="form-control {{ $errors->has('direccion_destinatario') ? ' is-invalid' : '' }}" value="{{ old('direccion_destinatario') ? old('direccion_destinatario') : $guia->direccion_destinatario }}" placeholder="Dirección del destinatario" name="direccion_destinatario" type="text" maxlength="100" required autocomplete="off">
                    @if ($errors->has('nombre_destinatario'))
                    <div class="invalid-feedback">{{ $errors->first('direccion_destinatario') }}</div>
                    @endif
                </div>
            </div>
        </div>  


        <div class="row g-3 py-1">
            <div class="col">
                <div class="form-group">
                    {{--<label for="Teléfono del destinatario">Teléfono Del Destinatario</label>--}}
                    <input class="form-control {{ $errors->has('telefono_destinatario') ? ' is-invalid' : '' }}" value="{{ old('telefono_destinatario') ? old('telefono_destinatario') : $guia->telefono_destinatario }}" placeholder="Teléfono del destinatario" name="telefono_destinatario" type="text" maxlength="50" >
                    @if ($errors->has('telefono_destinatario'))
                    <div class="invalid-feedback">{{ $errors->first('telefono_destinatario') }}</div>
                    @endif
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    {{--<label for="Email del destinatario">Email Del Destinatario</label>--}}
                    <input class="form-control {{ $errors->has('email_destinatario') ? ' is-invalid' : '' }}" value="{{ old('email_destinatario') ? old('email_destinatario') : $guia->email_destinatario }}" placeholder="Email del destinatario" name="email_destinatario" type="email" maxlength="100">
                    @if ($errors->has('email_destinatario'))
                    <div class="invalid-feedback">{{ $errors->first('email_destinatario') }}</div>
                    @endif
                </div>
            </div>
        </div>  
       
        
        <div class="row g-3 py-1">
            <div class="col">
                <div class="form-group">
                    <label for="Total">Total</label>
                    <input class="form-control {{ $errors->has('total') ? ' is-invalid' : '' }}" value="{{ old('total') ? old('total') : $guia->total }}" placeholder="Total" name="total" type="number" step="0.01" required>
                    @if ($errors->has('total'))
                    <div class="invalid-feedback">{{ $errors->first('total') }}</div>
                    @endif
                </div> 
            </div>
            <div class="col"> 
                         
            </div>
        </div>                  
    </div>
    <div class="box-footer py-3">
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</div>   