@extends('layouts.layout_m')

@section('Facturacion')
    Crear Guia
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Crear Guía</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('guias.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf
                            @include('guia.form')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('page_script')
<script type="text/javascript">
    $(function () {
        $('#datetimepicker').datetimepicker({
            format:'Y-m-d H:i:s',
            theme:'dark',
            lang:'es',  
            startDate:'+2021/12/21',         
        });
    });
  </script>
@endsection