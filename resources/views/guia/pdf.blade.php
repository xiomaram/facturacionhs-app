@extends('layouts.export_pdf')
@section('title')
Guia
@endsection
@section('css')


<style>
   
   header { position: fixed; margin: 5mm 5mm; height: 50px; margin-top: 10px;}
   .table{      
      font-size: 12px;
      margin-bottom: 10px !important; 
      width: 100% !important;  	
      border: 1px;
      margin-top: 50%;
   }
   .table thead th {   
      
       font-size: 12px;   	
   }
   .table tfoot th {   
       border-bottom: 2px solid #D5B870;
       background-color: #fff; 
       font-size: 10px;     
     
   }
   .title{
       font-size: 20px;
       font-family: 'Exo' !important;
       color: #5F5F5F;
   } 
   </style>
@endsection
@section('header-title')
<div class="text-center">Guía</div>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <table style="margin-top: 50%, border:2px">
            <thead>	
                <td style="width: 40%">{{$guia->numero_guia}}</td>
                <td colspan="2">&nbsp;</td>                                    
                <td style="width: 40%">{{$guia->fecha_envio}}</td>							
            </thead>
            <tbody style="margin-top: 50px">
                @if($guia)  
                <tr>
                    <td colspan="2">País Origen</td>
                    <td colspan="2">País Destino</td>                                   
                </tr>
                <tr>
                    <td colspan="2">{{$guia->pais_origen}}</td>
                    <td colspan="2">{{$guia->pais_destino}}</td>                    
                </tr>
                <tr>
                    <td colspan="2">Nombre del remitente</td>
                    <td colspan="2">Nombre del destinatario</td>                    
                </tr>
                <tr>
                    <td colspan="2">{{$guia->nombre_remitente}}</td>
                    <td colspan="2">{{$guia->nombre_destinatario}}</td>
                </tr>
                <tr>
                    <td colspan="2">Dirección del remitente</td>
                    <td colspan="2">Dirección del destinatario</td>
                </tr>
                <tr>
                    <td colspan="2">{{$guia->direccion_remitente}}</td>
                    <td colspan="2">{{$guia->direccion_destinatario}}</td>
                </tr>
                <tr>
                    <td colspan="2">Teléfono del remitente</td>
                    <td colspan="2">Telefono del destinatario</td>
                </tr>
                <tr>
                    <td colspan="2">{{$guia->telefono_remitente}}</td>
                    <td colspan="2">{{$guia->telefono_destinatario}}</td>
                </tr>
                <tr>
                    <td colspan="2">Email del remitente</td>
                    <td colspan="2">Email del destinatario</td>                                    
                </tr> 
                <tr>
                    <td colspan="2">{{$guia->email_remitente}}</td>
                    <td colspan="2">{{$guia->email_destinatario}}</td>                                   
                </tr> 
                @else
                <tr>                                    
                         <td colspan="4">Guía No registrada</td>
                      
                </tr>
                @endif 
            </tbody>
            <tfoot>	
            </tfoot>							
        </table>		
        
    </div>
</div>

            
						
<!-- END PAGE CONTENT-->	
@endsection