@extends('layouts.layout_m')

@section('template_title')
    {{ $guia->name ?? 'Mostrar Guía' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Guía</span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a class="btn btn-primary" href="{{ route('guias.index') }}"> Volver a Guias</a>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-4 d-flex justify-content-end">
                                    <a class="btn btn-primary" href="{{ route('guias.pdf',$guia->id) }}">Convertir a PDF</a>
                                </div>
                            </div>
                        </div>                        
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Número de guía:</strong>
                            {{ $guia->numero_guia }}
                        </div>
                        <div class="form-group">
                            <strong>Fecha de envio:</strong>
                            {{ $guia->fecha_envio }}
                        </div>
                        <div class="form-group">
                            <strong>País origen:</strong>
                            {{ $guia->pais_origen }}
                        </div>
                        <div class="form-group">
                            <strong>Nombreremitente:</strong>
                            {{ $guia->nombre_remitente }}
                        </div>
                        <div class="form-group">
                            <strong>Direccionremitente:</strong>
                            {{ $guia->direccion_remitente }}
                        </div>
                        <div class="form-group">
                            <strong>Telefonoremitente:</strong>
                            {{ $guia->telefono_remitente }}
                        </div>
                        <div class="form-group">
                            <strong>Emailremitente:</strong>
                            {{ $guia->email_remitente }}
                        </div>
                        <div class="form-group">
                            <strong>Paisdestino:</strong>
                            {{ $guia->pais_destino }}
                        </div>
                        <div class="form-group">
                            <strong>Nombredestinatario:</strong>
                            {{ $guia->nombre_destinatario }}
                        </div>
                        <div class="form-group">
                            <strong>Direcciondestinatario:</strong>
                            {{ $guia->direccion_destinatario }}
                        </div>
                        <div class="form-group">
                            <strong>Telefonodestinatario:</strong>
                            {{ $guia->telefono_destinatario }}
                        </div>
                        <div class="form-group">
                            <strong>Emaildestinatario:</strong>
                            {{ $guia->email_destinatario }}
                        </div>
                        <div class="form-group">
                            <strong>Total:</strong>
                            {{ $guia->total }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
