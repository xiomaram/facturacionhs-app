@extends('layouts.layout_m')

@section('template_title')
    Update Guia
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Actualizar Guía</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('guias.update', $guia->id) }}"  role="form" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf

                            @include('guia.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
