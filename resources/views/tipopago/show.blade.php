@extends('layouts.layout_m')

@section('template_title')
    {{ $tipopago->name ?? 'Mostrar Tipopago' }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Tipo de pago</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('tipopagos.index') }}"> Volver</a>
                        </div>
                    </div>

                    <div class="card-body">                        
                        <div class="form-group">
                            <strong>Descripcion:</strong>
                            {{ $tipopago->descripcion }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection