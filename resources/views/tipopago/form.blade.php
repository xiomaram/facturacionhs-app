<div class="box box-info padding-1">
    <div class="box-body">
        <div class="col py-2">
            <div class="form-group">
                {{--<label for="Tipo de Pago">Tipo de pago</label>--}}
                <input class="form-control {{ $errors->has('descripcion') ? ' is-invalid' : '' }}" value="{{ old('descripcion') ? old('descripcion') : $tipopago->descripcion }}" placeholder="Tipo de pago" name="descripcion" type="text" required autocomplete="off" minlength="1" maxlength="100">
                @if ($errors->has('descripcion'))
                <div class="invalid-feedback">{{ $errors->first('descripcion') }}</div>
                @endif
            </div>              
        </div>   
    </div>
    <div class="box-footer mt30">
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</div>