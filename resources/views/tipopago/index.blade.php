@extends('layouts.layout_m')

@section('template_title')
    Tipos de pago
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Tipopago') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('tipopagos.create') }}" class="btn btn-facture-1 btn-sm float-right"  data-placement="left">
                                  {{ __('Crear Tipo') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th >N°</th>                                        
										<th style="min-width: 600px";>Descripción</th>
                                        <th style="min-width: 150px">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($tipopagos as $tipopago)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $tipopago->descripcion }}</td> 
                                            <td>
                                                <div class="row">
                                                    <div class="col-2"> 
                                                        <a class="btn  text-primary " href="{{ route('tipopagos.show',$tipopago->id) }}"><i class="fa fa-fw fa-eye"></i></a>
                                                    </div>
                                                    <div class="col-2"> 
                                                        <a class=" btn text-success" href="{{ route('tipopagos.edit',$tipopago->id) }}"><i class="fa fa-fw fa-edit"></i></a>
                                                    </div>
                                                    <div class="col-2">                                            
                                                        <form action="{{ route('tipopagos.destroy',$tipopago->id) }}" method="POST">                                                    
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn text-danger "><i class="fa fa-fw fa-trash"></i></button>
                                                        </form>
                                                    </div>
                                                </div>

        
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $tipopagos->links() !!}
            </div>
        </div>
    </div>
@endsection