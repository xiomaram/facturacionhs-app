<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>{{ config('app.name') }} | @yield('title')</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Facturación-hs">
    <link href="{{url("/css/custom.css")}}" rel="stylesheet">
  

    <!-- BEGIN GLOBAL MANDATORY STYLES -->

    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE STYLES -->
    @yield('css')    
    <!-- END PAGE STYLES -->
</head>
<body>
<header>
	<div class="row">
    	<div class="col-md-12">    		
    		@yield('header-title')
    		<h5 style="text-align: right"><b>Impreso el {{ date('d/m/Y') }}</b></h5>    		
    	</div>
	</div>
</header>
<footer>
@yield('footer')
</footer>
<!-- BEGIN CONTAINER -->
<main>
	<!-- BEGIN CONTENT -->
	@yield('content')
	<!-- END CONTENT -->
</main>
<!-- END CONTAINER -->

</body>
@yield('script')
</html>
