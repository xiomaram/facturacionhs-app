<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{env("APP_NAME")}}">
    <meta name="author" content="Xiomara">
    <title>{{env("APP_NAME")}} - @yield("titulo")</title>
    <link href="{{ url('bootstrap-5.1.3/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{url("/css/all.min.css")}}" rel="stylesheet">
    <link href="{{url("/css/custom.css")}}" rel="stylesheet">
    <!--datetimepicker-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" integrity="sha512-bYPO5jmStZ9WI2602V2zaivdAnbAhtfzmxnEGh9RwtlI00I9s8ulGe4oBa5XxiC6tCITJH/QG70jswBhbLkxPw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  </head>
<body> 
        <header class="navbar navbar-expand-md navbar-dark bg-dark fixed-top menu-general">
            <a class="navbar-brand col-md-3 col-lg-2 me-0 px-2" href="#">Facturación</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
            id="botonMenu" aria-label="Mostrar u ocultar menú">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class=" ms-auto" >
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Registro</a>
                    </li>
                </ul>
            </div>
        </header>
         
        <div class="container-fluid " >
          <div class="row">
            <nav id="menu" class="col-md-3 col-lg-2 d-md-block bg-dark sidebar collapse  menu-general2">
              <div class="position-sticky pt-3">
                <ul class="nav flex-column">
                  <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{route("home")}}">                      
                      <i class="fas fa-home"></i> Inicio
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{route("tipopagos.index")}}">                      
                      <i class="far fa-credit-card"></i> Tipos de pago
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{route("guias.index")}}">                      
                      <i class="fas fa-clipboard-list"></i> Guías
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{route("facturas.index")}}">                      
                      <i class="fas fa-file-invoice-dollar"></i> Facturas
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">                      
                      <i class="fas fa-print"></i> Reportes
                    </a>
                  </li>              
                </ul>
              </div>
            </nav>
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-2 py-5 content-or">       
                    @yield("content")             
            </main>
          </div>
        </div>
    <!-- Menu2 fin-->



<footer class="px-2 py-2 fixed-bottom bg-dark">
    <span class="text-muted">Facturación      
    </span>
</footer>
<script src="{{url("/js/all.min.js")}}" type="text/javascript"></script>
<script src="{{ url('/jquery-3.6/jquery-3.6.0.min.js')}}"></script> 
<script src="{{ url('/bootstrap-5.1.3/js/bootstrap.min.js')}}"></script> 

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", () => {
       const menu = document.querySelector("#menu"),
           botonMenu = document.querySelector("#botonMenu");
       if (menu) {
           botonMenu.addEventListener("click", () => menu.classList.toggle("show"));
       }
   });
</script>
<!-- Formato de hora -->
<script src="https://cdn.bootcss.com/moment.js/2.18.1/moment-with-locales.min.js"></script>
<!--datetimepicker-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.0/build/jquery.datetimepicker.full.min.js"></script>
@yield('page_script')
</body>
</html>
