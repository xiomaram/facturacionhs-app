<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    use App\Http\Controllers\GuiaController;
    use App\Http\Controllers\HomeController;
    use App\Http\Controllers\FacturaController;
    use App\Http\Controllers\TipoPagoController;


    Route::get('/', [HomeController::class, 'index'])->name('home');

    Route::get('/tipopago/index', [TipoPagoController::class, 'index'])->name('tipopagos.index');
    Route::get('tipopago/create', [TipoPagoController::class, 'create'])->name('tipopagos.create');
    Route::post('tipopago/store', [TipoPagoController::class, 'store'])->name('tipopagos.store');
    Route::get('tipopago/show/{id}', [TipoPagoController::class, 'show'])->name('tipopagos.show');
    Route::get('tipopago/edit/{id}', [TipoPagoController::class, 'edit'])->name('tipopagos.edit');
    Route::put('tipopago/{id}', [TipoPagoController::class, 'update'])->name('tipopagos.update');
    Route::delete('tipopago/destroy/{id}', [TipoPagoController::class, 'destroy'])->name('tipopagos.destroy');


     /*
    /-----------------------------------------------------------------
    /Rutas del módulo Guías   
    /------------------------------------------------------------------
    */
    Route::get('/guia/index', [GuiaController::class, 'index'])->name('guias.index');
    Route::get('guia/create', [GuiaController::class, 'create'])->name('guias.create');
    Route::post('guia/store', [GuiaController::class, 'store'])->name('guias.store');
    Route::get('guia/show/{id}', [GuiaController::class, 'show'])->name('guias.show');
    Route::get('guia/edit/{id}', [GuiaController::class, 'edit'])->name('guias.edit');
    Route::put('guia/{id}', [GuiaController::class, 'update'])->name('guias.update');
    Route::delete('guia/destroy/{id}', [GuiaController::class, 'destroy'])->name('guias.destroy');
    Route::get('/guia/pdf/{id}', [GuiaController::class, 'createGuiaPDF'])->name('guias.pdf');

    /*
    /-----------------------------------------------------------------
    /Rutas del módulo Facturas   
    /------------------------------------------------------------------
    */

    Route::get('/factura/index', [FacturaController::class, 'index'])->name('facturas.index');
    Route::get('factura/create', [FacturaController::class, 'create'])->name('facturas.create');
    Route::get('factura/show/{id}', [FacturaController::class, 'show'])->name('facturas.show');    
    Route::get('factura/edit/{id}', [FacturaController::class, 'edit'])->name('facturas.edit');
    Route::delete('factura/destroy/{id}', [FacturaController::class, 'destroy'])->name('facturas.destroy');
    Route::put('factura/{id}', [FacturaController::class, 'update'])->name('facturas.update');
    /*Route::post('factura/store', [FacturaController::class, 'store'])->name('facturas.store');*/

    


    /*
    /-----------------------------------------------------------------
    /Facturar Guías    
    /------------------------------------------------------------------
    */
    //Route::get("/facturacion", "FacturaController@totalFactura")->name("facturas.totalFactura");
    Route::post("/facturarCancelar", [FacturaController::class, 'facturarCancelar'])->name("facturas.facturarCancelar");
    Route::post("/guiFactura", [FacturaController::class, 'agregarGuiaALista'])->name("facturas.agregarGuia");
    Route::delete("/guiaFactura", [FacturaController::class, 'quitarGuiaDeLista'])->name("facturas.quitarGuia");

    Route::post("/guiFacturaEdit", [FacturaController::class, 'agregarGuiaAListaEdit'])->name("facturas.agregarGuiaEdit");
    //Route::post("/guiFacturaEdit", [FacturaController::class, 'quitarGuiaDeListaEdit'])->name("facturas.quitarGuiaEdit");
 

  